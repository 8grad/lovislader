 LovisLoader™
------------
|   |   |   |
|---|---|---|
|![lovis loader](./images/00.jpg)|![lovis loader](./images/01.jpg)|![lovis loader](./images/02.jpg)|
|![lovis loader](./images/03.jpg)|![lovis loader](./images/04.jpg)|![lovis loader](./images/05.jpg)|

Box made of 5mm MDF. (Medium-density fibreboard)

|Front|Back|
|---|---|
|__front.svg, (5 mm MDF, brown, 790x384 mm ![lovis loader](./plans/__front.svg)| __sidesANDback.svg, (5 mm MDF, brown, 790x384 mm ![lovis loader](./plans/__sidesANDback.svg)

* Planed: https://www.makercase.com
* Lasered: https://www.formulor.de

As solar charge controller we are using a Victron MPPT 75/10. 
https://www.victronenergy.de/upload/documents/Datasheet-Blue-Solar-Charge-Controller-MPPT-75-10,-75-15-&-MPPT-100-15-DE.pdf
It can handle a panel with up to 75 Watts and has a max rated charge current of 10 Amps.
The Victron MPPT 75/10 has a serial port (VE.Direct), that communicates with an Arduino Mega Microcontroler. 
https://en.wikipedia.org/wiki/Arduino

Via VE.Direct Protocol the data is displayed through the Arduino and its small 2x40 characters IIC LCD display.
The VictronVEDirectArduino Library can be found here: https://github.com/winginitau/VictronVEDirectArduino

Via VictronConnectApp and Bluetooth statistics can be read. Also the configuration for the charge controller can be made.

* Mac: https://apps.apple.com/de/app/victronconnect/id1084677271?mt=12
* iOS: https://apps.apple.com/de/app/victronconnect/id943840744
* Android: https://play.google.com/store/apps/details?id=com.victronenergy.victronconnect&hl=de

![lovis loader](./app.png)

The 10 USB ports are DCP (Dedicates Charge Ports) and deliver up to 1.5 Amps.

On the bottom of the device is a main switch which cuts the load from the charge controller.
Also there is an USB-Port for programming the Arduino, a XLR battery-connection, a DHT-22 and the MQ 135.

Internally there is a relay, that — in case of low battery — switches the device on to 24V on-board power supply.
(https://www.victronenergy.de/dc-dc-converters/orion-24-12-5-10)

The DHT-22 measures the temperature and the relative humidity.
The MQ-135 is a sensor for air quality.
Inside there is also a barometer that measures the change in air pressure.

The LCD displays the following:

~~~
TEMP HUMIDITY PRESSURE          32°C 44% 1017.74hPa
DAY MONTH YEAR HOUR MINUTE      25.07.2019 16:47 UTC
VOLTAGE BAT CURRENT BAT         BAT: 14.1V (-)4.43A (depending on charge state)
MODUL POWER & VOLTAGE           PV: 79W 15.9V
~~~

Delay 10s

~~~
Energy (wH) today
Max Power (W) today
~~~

Delay 4s

~~~
Energy (wH) yesterdday
Max Energy (W) yesterdday
~~~

Delay 4s
~~~
Air Pressure (hPa)
~~~

Delay 4s
~~~
Air Quality 0-1000?!
~~~

LovisLader™ Schema
------------

![lovis loader](./plans/scheme.png)

LovisLader™ Electricity Demand
------------
> https://www.crosli.de/solaranlage-wohnmobil-berechnen/

	Avg. iPhone7 has a battery capacity of 1.960 mAh * 5V ~ 9.5Wh (Watt Hours)
	https://www.sir-apfelot.de/apple-iphone-akku-mah-alle-modelle-19102/
	
	/ or
	
	30 people use 9,5 Watt 60 Minuten a day. 
	== 297wh  (Watt Hours/ Day)
	
on a 12V system that results in:
	
	Dayly requirement: approx. 24Ah
	minimal battery capacity with 50% discharge:        49Ah with GEL/ AGM battery
	3 days of power self-sufficiency in bad weather:   148Ah with GEL/ AGM battery
                                                        87Ah with a Lithium	battery
	Solar capacity, to gain dayly usage:
	-- 58   Wp in Summer (Middle- and South Europe)
	-- 495  WP in Winter (Germany)
	-- 137  Wp in Winter (South-Spain, South-Portugal) 
	-- 107  Wp in Winter (Agadir, Marocco)

--

🏴
Marc Wright
marc@lovis.de