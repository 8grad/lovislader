#include "VEDirect.h"           // Victron ARDUINO lib

// 32 bit INTs to collect the data from the device
int32_t VE_soc, VE_power, VE_panelPower, VE_panelVoltage, VE_voltage, VE_current, VE_data, VE_yieldToday, VE_yieldYesterday, VE_MaxPowerToday, VE_MaxPowerYesterday;

// Boolean to collect an ON/OFF value
uint8_t VE_alarm, VE_load;

// VEDirect instantiated with relevant serial object
VEDirect myve(Serial1); // make sure you us a Mega 2560 for a serial port


void setup() {
  Serial.begin(19200);
  Serial1.begin(19200);
}

void loop() {
  // Read the data
    Serial.println("Reading values from Victron Energy device using VE.Direct");
    Serial.print("myve.begin(): "); Serial.println(myve.begin());

    if (Serial1.available()) {
      Serial.print("reading Victron...");
      VE_soc = myve.read(VE_SOC);
      VE_power = myve.read(VE_POWER);
      VE_voltage = myve.read(VE_VOLTAGE);
      VE_current = myve.read(VE_CURRENT);

      VE_alarm = myve.read(VE_ALARM);

      Serial.println(VE_alarm);
      Serial.println(VE_load);

    } else {
      Serial.println("Victron seems to be offline...");
      while (1);
    }

}
