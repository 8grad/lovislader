#ifndef LOVISLOADER_H_
#define LOVISLOADER_H_

/* SPECIAL CHARACTERS */
byte lovis[] = {
  B00010,
  B00110,
  B00010,
  B11010,
  B01010,
  B01010,
  B11111,
  B11110
};

byte wave[] = {
  B00000,
  B00000,
  B10001,
  B11011,
  B11111,
  B11111,
  B11111,
  B11111
};

byte phi[] = {
  B00100,
  B01110,
  B10101,
  B10101,
  B10101,
  B10101,
  B01110,
  B00100
};


void greet(void) {
  Serial.println("");
  Serial.println("------------------------------------");
  Serial.println("Hello Folks and welcome to LovisLoader™:");
  Serial.println("------------------------------------");
}


#endif /* LOVISLOADER_H_ */
