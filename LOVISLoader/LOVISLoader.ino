#include <LiquidCrystal_I2C.h>  // LCD lib
#include <Wire.h>               // i2c lib
#include <Adafruit_Sensor.h>    // 
#include <Adafruit_BMP085_U.h>  // barometer
#include <DHT.h>                // temp lib

#include <virtuabotixRTC.h>     // clock lib
#include <DS1307RTC.h>          // a basic DS1307 library that returns time as a time_t
#include <TimeLib.h>            // time/ date calculations

#include "Arduino.h"            //
#include "VEDirect.h"           // Victron ARDUINO lib

#include "lovisLoader.h"

// 32 bit INTs to collect the data from the device
int32_t VE_soc, VE_power, VE_panelPower, VE_panelVoltage, VE_voltage, VE_current, VE_data, VE_yieldToday, VE_yieldYesterday, VE_MaxPowerToday, VE_MaxPowerYesterday;

// Boolean to collect an ON/OFF value
uint8_t VE_alarm, VE_load;



// VEDirect instantiated with relevant serial object
VEDirect myve(Serial1);

// setup liquid crystal display chars and lines (20/4)
LiquidCrystal_I2C lcd(0x27, 20, 4);

// setup Adafruit_BMP085 Barometric Pressure + Temp sensor
Adafruit_BMP085_Unified bmp = Adafruit_BMP085_Unified(10085);

#define DHTPIN 11
#define DHTTYPE DHT22

// DHT sensor
DHT dht(DHTPIN, DHTTYPE);



/*************** THE AMP SENSOR *****************************/
/**
   30A Version: Chip ACS712ELCTR-30A-T
   Spannungsversorgung: 5V | Messbereich: -30A bis +30A |
   Spannungsänderung am Sensor pro 1A: 66mV
*/
int currentSensor = A2;   // Sensor is connected to Pin A2 (Analog "2") .
int VpA = 66;             // Millivolt per Ampere (100 for 20A Modul and 66 for 30A Modul)
int sensorValue = 0;
int nullpunkt = 2550;     // voltage in mV where there is no current
double sensorVoltage = 0;
double ampere = 0;

/*************** THE AIR SENSOR *****************************/
#define gasSensor A1  //sensor on Analog 1
int gasLevel = 0;     //int variable for gas level
String quality = "";

/*************** THE CLOCK *************************/

// CLK    (yellow)  -> 3
// DAT    (orange)  -> 12
// Reset  (brown)   -> 13
//
virtuabotixRTC myRTC(3, 12, 13); //CLK/DAT/RESET

/*************** THE MPPT *************************/
/* Connections:
    MPPT pin     MPPT        Arduino     Arduino pin
    1            GND         GND         GND
    2            RX          TX          -              do not use!
    3            TX          RX          19 (MEGA)
    4            Power+      none        -              do not use!
*/
// VE Direct Plug
// ┏━━     ━━┓
// ┃ 1|2|3|4 ┃
// ┗━━━━━━━━━┛
// 1. GND
// 2. VE.Direct-RX - connects to Arduino TX1
// 3. VE.Direct-TX - connects to Arduino RX1
// 4. Power +
void displaySensorDetails(void)
{
  sensor_t sensor;
  bmp.getSensor(&sensor);
  Serial.println("------------------------------------");
  Serial.print  ("Sensor:       "); Serial.println(sensor.name);
  Serial.print  ("Driver Ver:   "); Serial.println(sensor.version);
  Serial.print  ("Unique ID:    "); Serial.println(sensor.sensor_id);
  Serial.print  ("Max Value:    "); Serial.print(sensor.max_value); Serial.println(" hPa");
  Serial.print  ("Min Value:    "); Serial.print(sensor.min_value); Serial.println(" hPa");
  Serial.print  ("Resolution:   "); Serial.print(sensor.resolution); Serial.println(" hPa");
  Serial.println("------------------------------------");
  Serial.println("");
  delay(1000);
}

void setup() {
  Serial.begin(19200);
  Serial1.begin(19200);

  /* gas sensor for input */
  pinMode(gasSensor, INPUT);

  /* Initialise the sensor */
  if (!bmp.begin())
  {
    /* There was a problem detecting the BMP085 ... check your connections */
    Serial.print("Ooops, no BMP085 detected ... Check your wiring or I2C ADDR!");
    //while (1);
  }

  /* Display welcome message */
  greet();

  /* Display some basic information on this sensor */
  displaySensorDetails();


  lcd.begin(); //Im Setup wird der LCD gestartet
  dht.begin(); //Im Setup wird der DHT22 gestartet

  lcd.createChar(0, lovis);
  lcd.createChar(2, wave);
  lcd.home();
  lcd.write(byte(0));
  lcd.print(" hello, lovis!");
  lcd.setCursor(0, 1);

  for (int i = 0; i <= 19; i++) {
    lcd.write(byte(2));
  }

  delay(1000);
  lcd.setCursor(0, 2);


  // setSyncProvider(RTC.get);   // the function to get the time from the RTC
  // if (timeStatus() != timeSet)
  // Serial.println("Unable to sync with the RTC");
  // else
  // Serial.println("RTC has set the system time");
  // Set the current date, and time in the following format:
  // seconds, minutes, hours, day of the week, day of the month, month, year
  // myRTC.setDS1302Time(0, 34, 13, 5, 13, 12, 2019); //Here you write your actual time/date as shown above
  // but remember to "comment/remove" this function once you're done
  // The setup is done only one time and the module will continue counting it automatically

  //lcd.clear();
}

void loop() {
  /* display Time and Date only */
  lcd.clear();
  lcd.print("fair winds LOVIS!");
  Serial.println("Reading values from Victron Energy device using VE.Direct text mode");
	Serial.println();

  // Read the MPPT Data
  if (myve.begin()) { // test connection
    VE_soc = myve.read(VE_SOC);
    VE_power = myve.read(VE_POWER);
    VE_panelPower = myve.read(VE_PANELPOWER);
    VE_panelVoltage = myve.read(VE_PANELVOLTAGE);
    VE_voltage = myve.read(VE_VOLTAGE);
    VE_current = myve.read(VE_CURRENT);

    VE_alarm = myve.read(VE_ALARM);
    VE_load = myve.read(VE_LOAD);

    VE_yieldYesterday = myve.read(VE_YIELD_YESTERDAY);
    VE_yieldToday = myve.read(VE_YIELD_TODAY);
    VE_MaxPowerYesterday = myve.read(VE_MAXPOWER_YESTERDAY);
    VE_MaxPowerToday = myve.read(VE_MAXPOWER_TODAY);

    Serial.println(VE_alarm);
    Serial.println(VE_load);    
  } else {
    Serial.println("Could not open serial port to VE device");
    while (1);
  }

  lcd.clear();
  lcd.setCursor(0, 2);
  lcd.print("BAT: ");
  float v = VE_voltage;
  lcd.print((v / 1000), 1);
  lcd.print("V ");
  float a = VE_current;
  lcd.print((a / 1000), 2);
  lcd.print("A");
  float w = VE_panelPower;
  lcd.setCursor(0, 3);
  lcd.print("PV: ");
  lcd.print(w, 1);
  lcd.print("W ");
  float vpv = VE_panelVoltage;
  lcd.print((vpv / 1000), 1);
  lcd.print("V");

  delay(10000);
  lcd.clear();

  //Serial.println("All data from device:");
	//myve.read(VE_DUMP);
	//Serial.println();

  /* read gas sensor */
  gasLevel = analogRead(gasSensor);
  Serial.print("Gas: "); Serial.println(gasLevel);

  /* read current sensor */
  sensorValue = analogRead(currentSensor) ;
  sensorVoltage = (sensorValue / 1023.0) * 5120;  // Hier wird der Messwert in den Spannungswert am Sensor umgewandelt. sensor/ resolution * VCC mV
  ampere = ((sensorVoltage - nullpunkt) / VpA);   // Im zweiten Schritt wird hier die Stromstärke berechnet.

  Serial.println("----------------------------------");
  Serial.print("Current Sensor Value: " );  // Print pure sensor value
  Serial.print(sensorValue);
  Serial.println("");
  Serial.print("Sensor Voltage: ");         // Print sennsor Voltage
  Serial.print(sensorVoltage);
  Serial.print(" mV");
  Serial.println("");
  Serial.print("Amps:");                 // shows the current measured
  Serial.print(ampere, 2);               // 2 == decimal points
  Serial.println("");
  /* update the time*/
  myRTC.updateTime();

  Serial.println("----------------------------------");
  Serial.println("waiting for time sync of processSyncMessage… ");
  Serial.println("to set time send UNIX TimeStamp '$date +%s' with prefix T through serial connection");
  Serial.println("eg.: T1576511954");
  Serial.println("----------------------------------");

  if (Serial.available()) {
    time_t t = processSyncMessage();
    Serial.println(t);
    if (t != 0) {
      Serial.println("setting up the time");
      // seconds, minutes, hours, day of the week, day of the month, month, year
      // myRTC.setDS1302Time(0, 34, 13, 5, 13, 12, 2019); //Here you write your actual time/date as shown above
      // RTC.set(t);   // set the RTC and the system time to the received value
      // setTime(t);
      // https://github.com/PaulStoffregen/Time

      RTC.set(t);   // set the RTC and the system time to the received value
      setTime(t);

      myRTC.setDS1302Time(second(), minute(), hour(), weekday(), day(), month(), year());
      myRTC.updateTime();
      Serial.print("Day:"); Serial.println(myRTC.dayofmonth);
      Serial.print("Month:"); Serial.println(myRTC.month);
      Serial.print("Year:"); Serial.println(myRTC.year);
      Serial.print("Hours:"); Serial.println(myRTC.hours);
      Serial.print("Minutes:"); Serial.println(myRTC.minutes);
    }
  }

  float humidity_data = dht.readHumidity();
  float temperature_data = dht.readTemperature();

  /* charge state */
  if (VE_load == 0 && VE_voltage == 0 )
  {
    lcd.print("on-board power used!");
    lcd.setCursor(0, 1);
    lcd.print("SENS: ");
    lcd.print(sensorVoltage);
    lcd.print("mV");
    lcd.setCursor(0, 2);
    lcd.print("BORD A: ");
    lcd.print(ampere, 2);
    lcd.print("A");
    lcd.setCursor(0, 3);
    lcd.print("USB: ");
    lcd.print(ampere - 0.4, 1 * 20 / 5.1); 
    /** 
     * todo: we need a Voltage sensor for both values: 
     * Volts Battery boat and VCC 
     * 
      */
    lcd.print("A");
  }

  delay(2000);
  lcd.clear();

  /* Get a new sensor event */
  sensors_event_t event;
  bmp.getEvent(&event);



  /* display yesterdays energy and watts only */
  lcd.clear();
  lcd.print("Energy yesterday:");
  lcd.setCursor(0, 1);
  lcd.print(VE_yieldYesterday * 0.01 * 1000 );
  lcd.print("Wh");
  lcd.setCursor(0, 2);
  lcd.print("Max power yesterday:");
  lcd.setCursor(0, 3);
  lcd.print(VE_MaxPowerYesterday);
  lcd.print("W");
  delay(4000);
  lcd.clear();

  /* display today energy and watts only */
  lcd.print("Energy today:");
  lcd.setCursor(0, 1);
  lcd.print((int)VE_yieldToday * 0.01 * 1000 );
  lcd.print("Wh");
  lcd.setCursor(0, 2);
  lcd.print("Max power today:");
  lcd.setCursor(0, 3);
  lcd.print(VE_MaxPowerToday);
  lcd.print("W");
  delay(4000);
  lcd.clear();

  /* display Air Quality only */
  lcd.print("Air Quality:");
  lcd.setCursor(0, 1);
  if (gasLevel)
  {
    /* Display atmospheric pressure in hPa */
    lcd.print(gasLevel);
    lcd.print("/ 1000");
  }

  delay(4000);
  lcd.clear();

  /* Display the results (barometric pressure is measure in hPa) */
  if (event.pressure)
  {
    lcd.clear();
    float pressure = event.pressure;
    /* Display atmospheric pressue in hPa */
    Serial.print("Pressure: ");
    Serial.print(pressure);
    Serial.println(" hPa");

    /* display pressure only */
    lcd.print("Pressure:");
    lcd.setCursor(0, 1);
    /* Display atmospheric pressue in hPa */
    lcd.print(pressure);
    lcd.print("hPa");
    lcd.setCursor(0, 2);
    lcd.print((float)pressure / 1000, 3) ;
    lcd.print(" Bar");
    delay(4000);
    lcd.clear();
  }

  char logDateString[130];
  lcd.print("Date/ Time:");
  lcd.setCursor(0, 1);
  sprintf(logDateString, "%02d.%02d.%04d %02d:%02d", myRTC.dayofmonth, myRTC.month, myRTC.year, myRTC.hours, myRTC.minutes);
  lcd.print(logDateString);
  
  delay(4000);
  lcd.clear();

  // combined display
  lcd.setCursor(0, 0);
  lcd.print(temperature_data, 0);
  lcd.print("\337C");
  lcd.print("/ ");
  lcd.print(humidity_data, 0);
  lcd.print("% ");

  if (event.pressure)
  {
    /* Display atmospheric pressure in hPa */
    lcd.print(event.pressure);
    lcd.println("hPa");
  }

  lcd.setCursor(0, 1);
  sprintf(logDateString, "%02d.%02d.%04d %02d:%02d", myRTC.dayofmonth, myRTC.month, myRTC.year, myRTC.hours, myRTC.minutes);
  lcd.print(logDateString);

  Serial.print("Time from RTC: ");
  Serial.print(myRTC.hours);
  Serial.print(":");
  Serial.print(myRTC.minutes);
  Serial.print(" ");
  Serial.print(myRTC.dayofmonth);
  Serial.print(".");
  Serial.print(myRTC.month);
  Serial.print(".");
  Serial.print(myRTC.year);

  Serial.println("");
  Serial.println("that's it folks; Have a great day!");
  Serial.println("----------------------------------");
  Serial.println("");

  lcd.setCursor(0, 2);
  lcd.print("BAT: ");
  //float v = VE_voltage;
  lcd.print((v / 1000), 1);
  lcd.print("V ");
  //float a = VE_current;
  lcd.print((a / 1000), 2);
  lcd.print("A");
  //float w = VE_panelPower;
  lcd.setCursor(0, 3);
  lcd.print("PV: ");
  lcd.print(w, 1);
  lcd.print("W ");
  //float vpv = VE_panelVoltage;
  lcd.print((vpv / 1000), 1);
  lcd.print("V");

  delay(10000);
  lcd.clear();
}

/*  code to process time sync messages from the serial port   */
#define TIME_HEADER  "T"   // Header tag for serial time sync message

unsigned long processSyncMessage() {
  unsigned long pctime = 0L;
  const unsigned long DEFAULT_TIME = 1357041600; // Jan 1 2013

  if (Serial.find(TIME_HEADER)) {
    pctime = Serial.parseInt();
    return pctime;
    if ( pctime < DEFAULT_TIME) { // check the value is a valid time (greater than Jan 1 2013)
      pctime = 0L; // return 0 to indicate that the time is not valid
    }
  }
  return pctime;
}
